import React, { Component } from 'react';
//import './App.scss';
import * as axios from 'axios';
import Formation from './components/Formation';
import Pusher from 'pusher-js';

const APP_KEY = "6a3acdaba86ad858948b";
const APP_CLUSTER = "eu"; 
const CHANNEL = "lineups"; 
const EVENT = "lineup-updated";

class App extends Component {
  
  constructor(props){
    super(props);
    this.state = {
      lineup: null
    }
  }
  componentDidMount(){
    const intialLineupEndpoint = "http://lineups.dev.fantech.io";
    const _this = this;
    
    axios.get(intialLineupEndpoint)
    .then(function (response) {
      _this.updateLineup(response.data);
      
    })
    .catch(function (error) {
    });
    
    this.subscribeToUpdates();
  }
  subscribeToUpdates = () => {
    const _this = this;
    
    const pusher = new Pusher(APP_KEY, {
      cluster: APP_CLUSTER,
    });
    
    const channel = pusher.subscribe(CHANNEL);
    
    channel.bind(EVENT, (data) => {_this.updateLineup(data)});
  }
  updateLineup = (data) => {
    this.setState({lineup: data});
  }
  render() {
    const {lineup} = this.state;
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">{lineup ? lineup.team : ""} Team Lineup</h1>
        </header>
        <div className="lineup-container">
          {lineup && <Formation players={lineup.players} formation={lineup.formation}/>}
        </div>
      </div>
    );
  }
}

export default App;
