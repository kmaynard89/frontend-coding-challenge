import React from 'react';
import ReactDOM from 'react-dom';
import{ shallow } from 'enzyme';
import PlayerRow from '../components/PlayerRow';
import Formation from '../components/Formation';
const mockData = require("./mockData.json");

it('displays correct number of <PlayerRow />s for given formation', () => {
    const FormationWrapper =  shallow(<Formation players={mockData.players} formation={mockData.formation}/>);
    expect(FormationWrapper.find(PlayerRow).length).toBe(4); // 442 formation should have 4 rows incl goalkeeper
});