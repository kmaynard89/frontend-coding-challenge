import React from 'react';
import ReactDOM from 'react-dom';
import Player from '../components/Player';
import{ shallow } from 'enzyme';
const mockData = require("./mockData.json");
import PlayerRow, {getPlayerForPosition} from '../components/PlayerRow';

it('retrieves correct player for position number', () => {
    let positionNum = 1;
    const hugoLloris = {
        "name": "Hugo Lloris",
        "formation_place": 1,
        "type": "Goalkeeper",
        "position": "GK"
    };
    expect(getPlayerForPosition(mockData.players, positionNum).name).toEqual(hugoLloris.name);

    positionNum = 7;
    const Eriksen = {
        "name": "Christian Eriksen",
        "formation_place": 7,
        "type": "Midfielder",
        "position": "RM"
    };

    expect(getPlayerForPosition(mockData.players, positionNum).name).toEqual(Eriksen.name);


});

it('displays correct number of players for given row', ()=>{
    let row = [2, 5, 6, 3];
    let PlayerRowWrapper =  shallow(<PlayerRow players={mockData.players} playerRow={row}/>);
    expect(PlayerRowWrapper.find(Player).length).toBe(4)
    PlayerRowWrapper.unmount();
    
    row = [null, 10, 9, null];
    PlayerRowWrapper =  shallow(<PlayerRow players={mockData.players} playerRow={row}/>);
    expect(PlayerRowWrapper.find(Player).length).toBe(2)
    PlayerRowWrapper.unmount();

})