import React from 'react';
import ReactDOM from 'react-dom';
import Player from '../components/Player';
import{ shallow } from 'enzyme';


it('correctly displays player details', () => {
    const playerOne = {
        "name": "Serge Aurier",
        "formation_place": 2,
        "type": "Defender",
        "position": "RB"
    };

    const PlayerWrapper1 = shallow(<Player player={playerOne} />);
    expect(PlayerWrapper1.find("p.name").text()).toContain("Serge Aurier");
    PlayerWrapper1.unmount();

    const playerTwo = {
        "name": "Jan Vertonghen",
        "formation_place": 6,
        "type": "Defender",
        "position": "CB"
    };

    const PlayerWrapper2 = shallow(<Player player={playerTwo} />);
    expect(PlayerWrapper2.find("p.name").text()).toContain("Jan Vertonghen");
    PlayerWrapper2.unmount();

    
});