import React from 'react';
import ReactDOM from 'react-dom';
import App from '../App';
import{ shallow, mount } from 'enzyme';
import Formation from '../components/Formation';

const mockFormation = require("./mockData.json");

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('displays <Formation /> if lineup is set', () => {
  const context = {
    lineup: mockFormation
  };
  const APP =  shallow(<App /> ,{ disableLifecycleMethods:true });
  
  expect(APP.find(Formation).length).toBe(0); 
  
  APP.setState({lineup:mockFormation});
  
  expect(APP.find(Formation).length).toBe(1); 
  APP.unmount();
});









