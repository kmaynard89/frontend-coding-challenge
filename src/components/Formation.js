import React, {Component} from 'react';
import PlayerRow from './PlayerRow';

export const possibleFormations = {
    "442": {
        "rows": [
            [null, 1, null],
            [2, 5, 6, 3],
            [7, 4, 8, 11],
            [null, 10, 9, null]
        ]
    },
    "4411":{
        "rows":[
            [null, 1, null],
            [2, 5, 6, 3],
            [7, 4, 8, 11],
            [null, 10, null],
            [null, 9, null],
        ]
    },
    "3421": {
        "rows": [
            [null, 1, null],
            [null, 6, 5, 4, null],
            [2, 7, 8, 3],
            [null, 10, 11, null],
            [null, 9, null]
        ]
    }
}

class Formation extends Component {
    constructor(props){
        super(props);
        this.state = {
            playerRows: []
        };
    }
    componentDidMount(){
        const {formation} = this.props;
        this.setState({playerRows: possibleFormations[formation].rows});
    }
    render(){
        const {players} = this.props;
        const {playerRows} = this.state;
        return (
            <div>
                {playerRows.map((playerRow, idx) => (
                    <PlayerRow key={idx}  playerRow={playerRow} players={players} />
                ))}
            </div>
        )
    }
}

export default Formation;