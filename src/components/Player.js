import React from 'react';

const Player = (props) => { 
    const {player} = props;
    return (
        <span className="player">
            <img src="/images/player-shirt.png" alt={player.name} />
            <p className="name" aria-label="Player info">{player.name} <span className="position">({player.position})</span></p>
        </span>
    )   
};

export default Player;