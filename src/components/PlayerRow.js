import React from 'react';
import Player from './Player';

export const getPlayerForPosition = (players, pos) => {
    return players.find((player) => player.formation_place === pos);
};
const PlayerRow = (props) => {
    const {players,playerRow} = props;
    return(
        <div className="player-row">
            {playerRow.map((row, i)=> row ? <Player key={i} player={getPlayerForPosition(players, row)} /> : null )}
        </div>
    )
}

export default PlayerRow;