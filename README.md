This project displays a football team's current line-up and is updated every 20s.

## Requirements
* You’ll need to have Node >= 6

## Set up
The below instructions will get you started

### Clone or download the project

`$ git clone https://kmaynard89@bitbucket.org/kmaynard89/frontend-coding-challenge.git`

`$ cd frontend-coding-challenge`

### install npm packages

`npm install`

### start the project

`yarn start` or `npm start`

In your browser, visit `localhost:3000` to see the project in action.

## Example
Here's an example of a lineup you'll see at `localhost:3000` when the project is correctly set up.

![](./public/images/screenshot.png)

## Tests
Inside the directory, run `yarn test` or `npm test` to run the tests.